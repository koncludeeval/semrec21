FROM ubuntu:20.10

RUN apt-get update
RUN apt-get install -y git nano wget unzip  default-jdk

COPY execution_script.sh .

RUN chmod a+x execution_script.sh
RUN sed -i 's/\r//g' execution_script.sh
RUN mkdir /data

CMD ["./execution_script.sh"]



