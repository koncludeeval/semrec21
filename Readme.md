Evaluation script for the Konclude submission for SemREC 2021

See also the Docker image `koncludeeval/semrec21` (https://hub.docker.com/repository/docker/koncludeeval/semrec21). By running 
```
docker run -v /path/to/output:/data -it koncludeeval/semrec21
```
the evaluation is executed and the evaluation results are stored in the local folder /`path/to/output`. See, e.g., /`path/to/output/ore-competition-framework/data/responses/competition-evaluations/ore2015comp-dl-konclude-compare-classification/evaluation-results` for evaluation charts and tables.
