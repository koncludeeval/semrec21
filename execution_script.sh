#!/bin/bash

cd /data


git clone https://github.com/andreas-steigmiller/ore-competition-framework.git
wget https://zenodo.org/record/18578/files/ore2015_sample.zip?download=1
unzip ore2015_sample.zip\?download\=1
wget https://zenodo.org/record/11145/files/ore2014-reasoners.zip?download=1
unzip -n ore2014-reasoners.zip?download=1 -d ore-competition-framework

wget https://github.com/konclude/Konclude/releases/download/v0.7.0-1138/Konclude-v0.7.0-1138-Linux-x64-GCC-Static-Qt5.12.10.zip
unzip Konclude-v0.7.0-1138-Linux-x64-GCC-Static-Qt5.12.10.zip
wget https://www.derivo.de/fileadmin/externe_websites/ext.derivo/KoncludeReleases/v0.6.1-527/Konclude-v0.6.1-527-Linux-x64-GCC4.3.2-Static-Qt4.8.5.zip
unzip Konclude-v0.6.1-527-Linux-x64-GCC4.3.2-Static-Qt4.8.5.zip


mkdir ore-competition-framework/data/ontologies/ore2015comp

echo "declare -i counter=200" >> copy_script.sh
echo "while read p && [ \$counter -ge 1 ]" >> copy_script.sh
echo "do" >> copy_script.sh
echo "	echo \"copying file \$counter with name \$p\"" >> copy_script.sh
echo "	counter=\$((\$counter-1))" >> copy_script.sh
echo "	p=\`echo \$p | sed 's/\\r//g'\`" >> copy_script.sh
echo "	file=\"./pool_sample/files/\$p\"" >> copy_script.sh
echo "	cp \$file ore-competition-framework/data/ontologies/ore2015comp" >> copy_script.sh
echo "done <pool_sample/pure_dl/classification/fileorder.txt" >> copy_script.sh

chmod a+x copy_script.sh
./copy_script.sh


cd ore-competition-framework/data/ontologies
mkdir owl2bench
cd owl2bench
wget -O OWL2DL-1.owl https://zenodo.org/record/4764368/files/OWL2DL-1.owl?download=1
wget -O OWL2DL-10.owl https://zenodo.org/record/4764368/files/OWL2DL-10.owl?download=1
wget -O OWL2EL-1.owl https://zenodo.org/record/4764368/files/OWL2EL-1.owl?download=1
wget -O OWL2EL-10.owl https://zenodo.org/record/4764368/files/OWL2EL-10.owl?download=1
wget -O OWL2QL-1.owl https://zenodo.org/record/4764368/files/OWL2QL-1.owl?download=1
wget -O OWL2QL-10.owl https://zenodo.org/record/4764368/files/OWL2QL-10.owl?download=1
wget -O OWL2RL-1.owl https://zenodo.org/record/4764368/files/OWL2RL-1.owl?download=1
wget -O OWL2RL-10.owl https://zenodo.org/record/4764368/files/OWL2RL-10.owl?download=1



cd ../../../..

cd ore-competition-framework
chmod a+x scripts/create-classification-queries.sh
scripts/create-classification-queries.sh

cp -r data/reasoners/konclude-linux/ data/reasoners/konclude-v0.7.0-linux/
sed -i 's/ReasonerName\tKonclude/ReasonerName\tKonclude v0.7.0/' data/reasoners/konclude-v0.7.0-linux/reasoner.dat
sed -i 's/OutputPathName\tkonclude-linux/OutputPathName\tkonclude-v0.7.0-linux/' data/reasoners/konclude-v0.7.0-linux/reasoner.dat
cp ../Konclude-v0.7.0-1138-Linux-x64-GCC-Static-Qt5.12.10/Binaries/Konclude data/reasoners/konclude-v0.7.0-linux/Konclude


cp -r data/reasoners/konclude-linux/ data/reasoners/konclude-v0.6.1-linux/
sed -i 's/ReasonerName\tKonclude/ReasonerName\tKonclude v0.6.1/' data/reasoners/konclude-v0.6.1-linux/reasoner.dat
sed -i 's/OutputPathName\tkonclude-linux/OutputPathName\tkonclude-v0.6.1-linux/' data/reasoners/konclude-v0.6.1-linux/reasoner.dat
cp ../Konclude-v0.6.1-527-Linux-x64-GCC4.3.2-Static-Qt4.8.5/Binaries/Konclude data/reasoners/konclude-v0.6.1-linux/Konclude

cp data/competitions/test-dl-classification-linux.dat data/competitions/ore2015comp-dl-konclude-compare-classification-linux.dat
sed -i 's/CompetitionName\tTest DL Classification/CompetitionName\tORE2015Comp DL Konclude Compare Classification/' data/competitions/ore2015comp-dl-konclude-compare-classification-linux.dat
sed -i 's/OutputPathName\ttest-dl-classification/OutputPathName\tore2015comp-dl-konclude-compare-classification/' data/competitions/ore2015comp-dl-konclude-compare-classification-linux.dat
sed -i 's/Reasoners\thermit-linux/Reasoners\tkonclude-v0.7.0-linux\tkonclude-v0.6.1-linux/' data/competitions/ore2015comp-dl-konclude-compare-classification-linux.dat
sed -i 's#QueryPathFilterString\tclassification/test/dl#QueryPathFilterString\tclassification/ore2015comp#' data/competitions/ore2015comp-dl-konclude-compare-classification-linux.dat
sed -i 's#ExecutionTimeout\t180000#ExecutionTimeout\t180000#' data/competitions/ore2015comp-dl-konclude-compare-classification-linux.dat
sed -i 's#ProcessingTimeout\t180000#ProcessingTimeout\t150000#' data/competitions/ore2015comp-dl-konclude-compare-classification-linux.dat






cp data/competitions/test-dl-classification-linux.dat data/competitions/owl2bench-konclude-compare-classification-linux.dat
sed -i 's/CompetitionName\tTest DL Classification/CompetitionName\tOWL2Bench Konclude Compare Classification/' data/competitions/owl2bench-konclude-compare-classification-linux.dat
sed -i 's/OutputPathName\ttest-dl-classification/OutputPathName\towl2bench-konclude-compare-classification/' data/competitions/owl2bench-konclude-compare-classification-linux.dat
sed -i 's/Reasoners\thermit-linux/Reasoners\tkonclude-v0.7.0-linux\tkonclude-v0.6.1-linux/' data/competitions/owl2bench-konclude-compare-classification-linux.dat
sed -i 's#QueryPathFilterString\tclassification/test/dl#QueryPathFilterString\tclassification/owl2bench#' data/competitions/owl2bench-konclude-compare-classification-linux.dat
sed -i 's#ExecutionTimeout\t180000#ExecutionTimeout\t180000#' data/competitions/owl2bench-konclude-compare-classification-linux.dat
sed -i 's#ProcessingTimeout\t180000#ProcessingTimeout\t150000#' data/competitions/owl2bench-konclude-compare-classification-linux.dat




chmod a+x scripts/evaluate-competition.sh
scripts/evaluate-competition.sh data/competitions/ore2015comp-dl-konclude-compare-classification-linux.dat
scripts/evaluate-competition.sh data/competitions/owl2bench-konclude-compare-classification-linux.dat




